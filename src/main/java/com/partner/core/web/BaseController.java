package com.partner.core.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * 包      名：  com.partner.core.web
 * 创 建 人：   寻欢
 * 创建时间：  2016/9/19 15:22
 * 修 改 人：
 * 修改日期：
 */
public abstract class BaseController {
	/**
	 * 日志对象
	 */
	protected Logger logger = LoggerFactory.getLogger(getClass());
}
